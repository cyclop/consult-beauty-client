(function() {
  "use strict";

  var app = angular.module('consultBeauty', ['ionic', 'consultBeauty.utils', 'ngMessages', 'ion-autocomplete']);

  app.run(['$ionicPlatform', '$rootScope', '$timeout', '$state', '$user',
    function($ionicPlatform, $rootScope, $timeout, $state, $user) {
      ionic.Platform.ready(function() {
        ionic.Platform.isFullScreen = true;
      });

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;
        if (requireLogin && !$user.isAuthenticated()) {
          event.preventDefault();
          $state.go('login');
        }
      });

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        var stateName = toState.name;

        if (stateName == 'app') {
          $('.panel-button').removeAttr('inactive');
        }

        if (stateName.indexOf('app.blocks') == 0) {
          var blockName = stateName.replace('app.blocks.', '');

          $timeout(function() {
            $('.panel-button').attr('inactive', true);
            $('.panel-button[cb-action="' + blockName + '"]').removeAttr('inactive');
          }, 100);
        }
      });

    }
  ]);

  app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/app');

      $stateProvider
        .state('login', {
          url: '/login',
          views: {
            'main@': {
              templateUrl: 'pages/login.html',
              controller: 'LoginCtrl'
            }
          },
          data: {
            requireLogin: false
          }
        })
        .state('register', {
          url: '/register',
          views: {
            'main@': {
              templateUrl: 'pages/register.html',
              controller: 'RegisterCtrl'
            }
          },
          data: {
            requireLogin: false
          }
        })
        .state('app', {
          url: '/app',
          views: {
            'main@': {
              templateUrl: 'pages/home.html',
              controller: 'HomeCtrl'
            }
          },
          data: {
            requireLogin: true
          }
        })
        .state('app.account', {
          url: '/account',
          views: {
            'main@': {
              templateUrl: 'pages/account.html',
              controller: 'AccountCtrl'
            }
          }
        })
        .state('app.account.password', {
          url: '/password',
          views: {
            'main@': {
              templateUrl: 'pages/changepass.html',
              controller: 'ChangePassCtrl'
            }
          }
        })
        .state('app.subscriptions', {
          url: '/subscriptions',
          views: {
            'main@': {
              templateUrl: 'pages/subscriptions.html',
              controller: 'SubscriptionsCtrl'
            }
          }
        })
        .state('app.blocks', {
          abstract: true,
          url: '/blocks'
        })
        .state('app.blocks.client', {
          url: '/client',
          views: {
            'main@': {
              templateUrl: 'pages/client.html',
              controller: 'ClientCtrl'
            }
          }
        })
        .state('app.blocks.new', {
          url: '/new',
          views: {
            'main@': {
              templateUrl: 'pages/new.html',
              controller: 'NewCtrl'
            }
          }
        })
        .state('app.blocks.analysis', {
          url: '/analysis',
          views: {
            'main@': {
              templateUrl: 'pages/analysis.html',
              controller: 'AnalysisCtrl'
            }
          }
        })
        .state('app.blocks.prescription', {
          url: '/prescription',
          views: {
            'main@': {
              templateUrl: 'pages/prescription.html',
              controller: 'PrescriptionCtrl'
            }
          }
        })
        .state('app.blocks.programme', {
          url: '/programme',
          views: {
            'main@': {
              templateUrl: 'pages/programme.html',
              controller: 'ProgrammeCtrl'
            }
          }
        })
        .state('app.blocks.photos', {
          url: '/photos',
          views: {
            'main@': {
              templateUrl: 'pages/photos.html',
              controller: 'PhotosCtrl'
            }
          }
        })
        .state('app.blocks.history', {
          url: '/history',
          views: {
            'main@': {
              templateUrl: 'pages/history.html',
              controller: 'HistoryCtrl'
            }
          }
        })
        .state('app.blocks.notes', {
          url: '/notes',
          views: {
            'main@': {
              templateUrl: 'pages/notes.html',
              controller: 'NotesCtrl'
            }
          }
        })
        .state('app.blocks.instruct', {
          url: '/instruct',
          views: {
            'main@': {
              templateUrl: 'pages/instruct.html',
              controller: 'InstructCtrl'
            }
          }
        })
        .state('app.blocks.alerts', {
          url: '/alerts',
          views: {
            'main@': {
              templateUrl: 'pages/alerts.html',
              controller: 'AlertsCtrl'
            }
          }
        })
        .state('app.blocks.skinfp', {
          url: '/skinfp',
          views: {
            'main@': {
              templateUrl: 'pages/skinfp.html',
              controller: 'SkinfpCtrl'
            }
          }
        });
    }
  ]);

})();
