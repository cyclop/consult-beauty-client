(function () {
  "use strict";

  var utils = angular.module('consultBeauty.utils', []);

  utils.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
      return {
        'request': function (config) {
          config.headers = config.headers || {};

          var isAuth = $localStorage.getObject('isAuthenticated', {
            value: false
          }).value;

          if (isAuth) {
            config.headers.Authorization = 'Bearer ' + $localStorage.get('token');
          }

          return config;
        },
        'responseError': function (response) {
          /*
           if (response.status === 401 || response.status === 403) {
           $location.path('/signin');
           }
           return $q.reject(response);
           */
        }
      };
    }]);
  }]);

  utils.factory('$localStorage', ['$window', function ($window) {
    return {
      set: function (key, value) {
        $window.localStorage[key] = value;
      },
      get: function (key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      setObject: function (key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function (key) {
        return JSON.parse($window.localStorage[key] || '{}');
      }
    };
  }]);

  utils.factory('$guid', [function () {
    return function () {
      // RFC4122: The version 4 UUID is meant for generating UUIDs from truly-random or
      // pseudo-random numbers.
      // The algorithm is as follows:
      //     Set the two most significant bits (bits 6 and 7) of the
      //        clock_seq_hi_and_reserved to zero and one, respectively.
      //     Set the four most significant bits (bits 12 through 15) of the
      //        time_hi_and_version field to the 4-bit version number from
      //        Section 4.1.3. Version4
      //     Set all the other bits to randomly (or pseudo-randomly) chosen
      //     values.
      // UUID                   = time-low "-" time-mid "-"time-high-and-version "-"clock-seq-reserved and low(2hexOctet)"-" node
      // time-low               = 4hexOctet
      // time-mid               = 2hexOctet
      // time-high-and-version  = 2hexOctet
      // clock-seq-and-reserved = hexOctet:
      // clock-seq-low          = hexOctet
      // node                   = 6hexOctet
      // Format: xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
      // y could be 1000, 1001, 1010, 1011 since most significant two bits needs to be 10
      // y values are 8, 9, A, B
      var guidHolder = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
      var hex = '0123456789abcdef';
      var r = 0;
      var guidResponse = "";
      for (var i = 0; i < 36; i++) {
        if (guidHolder[i] !== '-' && guidHolder[i] !== '4') {
          // each x and y needs to be random
          r = Math.random() * 16 | 0;
        }

        if (guidHolder[i] === 'x') {
          guidResponse += hex[r];
        } else if (guidHolder[i] === 'y') {
          // clock-seq-and-reserved first hex is filtered and remaining hex values are random
          r &= 0x3; // bit and with 0011 to set pos 2 to zero ?0??
          r |= 0x8; // set pos 3 to 1 as 1???
          guidResponse += hex[r];
        } else {
          guidResponse += guidHolder[i];
        }
      }

      return guidResponse;
    };
  }]);

  utils.factory('$user', ['$localStorage', '$http', '$q', function ($localStorage, $http, $q) {
    var observerCallbacks = [];

    var apiBase = '';

    if (window.cordova) {
      apiBase = 'http://consult-beauty.test.home.noosxe.com:3000/api/v1';
    } else {
      apiBase = 'http://localhost:3000/api/v1';
    }

    var apiAuthURL = apiBase + '/auth';
    var apiRegisterURL = apiBase + '/register';
    var apiAppBaseURL = apiBase + '/app';
    var apiUserBaseURL = apiAppBaseURL + '/user';
    var apiUserUpdateURL = apiUserBaseURL + '/update';
    var apiPassUpdateURL = apiUserBaseURL + '/password/update';

    return {
      registerObserverCallback: function (cb) {
        observerCallbacks.push(cb);
      },
      deregisterObserverCallback: function (cb) {
        var index = observerCallbacks.indexOf(cb);

        if (index > -1) {
          observerCallbacks.splice(index, 1);
        }
      },
      notifyObserverCallbacks: function () {
        angular.forEach(observerCallbacks, function (callback) {
          callback();
        });
      },
      isAuthenticated: function () {
        return ($localStorage.getObject('isAuthenticated', {
          value: false
        }).value);
      },
      getCurrentUser: function () {
        return $localStorage.getObject('currentUser');
      },
      updateCurrentUser: function (user) {
        $localStorage.setObject('currentUser', user);

        $http.post(apiUserUpdateURL, {
          update: user
        }).then(function (response) {
          console.log(response);
        });
      },
      updatePassword: function (update, done) {
        $http.post(apiPassUpdateURL, {
          update: update
        }).then(function (response) {
          console.log(response);
        });
      },
      getClientLimit: function () {
        var currentUser = this.getCurrentUser();
        return currentUser.clientLimit || 5;
      },
      auth: function (email, password) {
        var deferred = $q.defer();

        $http.post(apiAuthURL, {
          email: email,
          password: password
        }).then(function (response) {
          if (!response || response.status != 200) {
            deferred.reject('error occurred');
            return;
          }

          var status = response.data.status;

          if (status == 'ok') {
            var token = response.data.token;
            var user = response.data.user;

            $localStorage.setObject('currentUser', user);
            $localStorage.set('token', token);
            $localStorage.setObject('isAuthenticated', {
              value: true
            });

            deferred.resolve(user);
          } else {
            deferred.reject(response.data.message);
          }
        }, function (err) {
          deferred.reject(err.message);
        });

        return deferred.promise;
      },
      signOut: function () {
        $localStorage.setObject('isAuthenticated', {
          value: false
        });
        $localStorage.setObject('currentUser', null);
        $localStorage.set('token', null);
      },
      register: function (user) {
        var deferred = $q.defer();

        $http.post(apiRegisterURL, {
          user: user
        }).then(function (response) {
          if (!response || response.status != 200) {
            deferred.reject('error occurred');
            return;
          }

          var token = response.data.token;
          var user = response.data.user;

          $localStorage.setObject('currentUser', user);
          $localStorage.set('token', token);
          $localStorage.setObject('isAuthenticated', {
            value: true
          });

          deferred.resolve(user);
        }, function (err) {
          deferred.reject(err.data);
        });

        return deferred.promise;
      }
    };
  }]);

  utils.factory('$clients', ['$localStorage', '$http', '$q', '$guid', '$user', '$rootScope',
    function ($localStorage, $http, $q, $guid, $user, $rootScope) {
      var observerCallbacks = [];
      var currentClient = null;

      var iface = {
        registerObserverCallback: function (cb) {
          observerCallbacks.push(cb);
        },

        deregisterObserverCallback: function (cb) {
          var index = observerCallbacks.indexOf(cb);

          if (index > -1) {
            observerCallbacks.splice(index, 1);
          }
        },

        notifyObserverCallbacks: function () {
          angular.forEach(observerCallbacks, function (callback) {
            callback();
          });
        },

        count: function () {
          return getClients().length;
        },

        getClientsWithNameHaving: function (query) {
          query = query.toLowerCase();
          var clients = getClients();

          return _.filter(clients, function(client) {
            return client.name.toLowerCase().indexOf(query) !== -1;
          });
        },

        setCurrent: function(client) {
          currentClient = client;
          $rootScope.currentClient = client;
        },

        getCurrent: function() {
          return currentClient;
        },

        newClient: function (data, done) {
          if (iface.count() == $user.getClientLimit()) {
            done({
              message: 'You have reached client limit!'
            });
            return;
          }

          data.uid = $guid();

          addClient(data);

          done();
        },

        removeClient: function (client, done) {
          removeClient(client);
          done();
        },

        saveClients: function () {
          saveClients();
        }
      };

      var clientsCache = null;

      var getClients = function () {
        clientsCache = clientsCache || ($localStorage.getObject('clients').clients || []);
        return clientsCache;
      };

      var saveClients = function () {
        var dataObj = {clients: clientsCache};
        $localStorage.setObject('clients', dataObj);
        iface.notifyObserverCallbacks();
      };

      var addClient = function (client) {
        getClients();
        clientsCache.push(client);
        saveClients();
      };

      var removeClient = function (client) {
        getClients();

        var uid = client.uid;

        clientsCache = _.filter(clientsCache, function(cl) {
          return cl.uid !== uid;
        });

        saveClients();
      };

      return iface;
    }]);

})();
