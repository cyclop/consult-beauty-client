(function () {
  "use strict";

  var app = angular.module('consultBeauty');

  app.controller('LoginCtrl', ['$scope', '$user', '$state', '$ionicPopup',
    function ($scope, $user, $state, $ionicPopup) {
      $scope.loginData = {};

      $scope.doLogin = function (form) {
        $user.auth(
          $scope.loginData.email,
          $scope.loginData.password
        ).then(function (user) {
          $scope.loginData = {};
          $state.go('app');
        }, function (err) {
          $ionicPopup.alert({
            title: 'Login failed',
            template: 'Sorry, login failed!\nPlease try again.'
          });
        });
      };
    }
  ]);

  app.controller('RegisterCtrl', ['$scope', '$user', '$state',
    function ($scope, $user, $state) {
      $scope.registerData = {};

      $scope.doSignup = function () {
        var user = angular.copy($scope.registerData);
        delete user.confirm;

        $user.register(user).then(function (result) {
          $state.go('app');
        }, function (err) {
          console.log(err);
        });
      };
    }
  ]);

  app.controller('HomeCtrl', ['$scope', '$ionicHistory',
    function ($scope, $ionicHistory) {
      $scope.$on('$ionicView.enter', function () {
        $ionicHistory.clearHistory();
      });
    }
  ]);

  app.controller('AccountCtrl', ['$scope', '$user', '$state',
    function ($scope, $user, $state) {
      $scope.$on('$ionicView.enter', function () {
        $scope.user = $user.getCurrentUser();
      });

      $scope.goToPassChange = function () {
        $state.go('app.account.password');
      };

      $scope.saveUser = function () {
        $user.updateCurrentUser($scope.user);
        $state.go('app');
      };
    }
  ]);

  app.controller('ChangePassCtrl', ['$scope', '$user', '$state',
    function ($scope, $user, $state) {
      $scope.$on('$ionicView.enter', function () {
        $scope.passData = {};
      });

      $scope.changePass = function () {
        $user.updatePassword($scope.passData, function () {
          $state.go('app.account');
        });
      };
    }
  ]);

  app.controller('SubscriptionsCtrl', ['$scope', '$user',
    function ($scope, $user) {

    }
  ]);

  app.controller('ClientCtrl', ['$scope', '$user', '$clients', '$ionicPopup', '$ionicActionSheet',
    function ($scope, $user, $clients, $ionicPopup, $ionicActionSheet) {
      $scope.$on('$ionicView.enter', function () {
        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });

      $scope.clientsByName = function (query) {
        return $clients.getClientsWithNameHaving(query);
      };

      $scope.clientChosen = function (callback) {
        $scope.vars.data = callback.item;
        $clients.setCurrent(callback.item);
      };

      $scope.clientUnchosen = function (callback) {
        $scope.vars.data = null;
        $clients.setCurrent(null);
      };

      // how heard chooser
      $scope.choose = function () {
        if (!$scope.vars.data) return;

        var buttons = [
          {text: 'Friend'},
          {text: 'Family'},
          {text: 'Work Colleague'},
          {text: 'Walk-in'},
          {text: 'Internet'},
          {text: 'Social media'}
        ];

        $ionicActionSheet.show({
          buttons: buttons,
          titleText: 'How did you hear about us?',
          cancelText: 'Cancel',
          cancel: function () {
          },
          buttonClicked: function (index) {
            $scope.data.howHear = buttons[index].text;
            return true;
          }
        });
      };

      $scope.delete = function () {
        $ionicPopup.confirm({
          title: 'Confirmation',
          template: 'Are you sure you want to delete this client ?'
        }).then(function (res) {
          if (res) {
            var currentClient = $clients.getCurrent();
            $clients.removeClient(currentClient, function () {
              $clients.setCurrent(null);
              $scope.vars.data = null;
              $scope.vars.client = null;
            });
          }
        });
      };
    }
  ]);

  app.controller('NewCtrl', ['$scope', '$user', '$clients', '$ionicActionSheet', '$ionicPopup', '$state',
    function ($scope, $user, $clients, $ionicActionSheet, $ionicPopup, $state) {
      $scope.$on('$ionicView.enter', function () {
        //$scope.data = {};

        $scope.data = {
          name: 'test name',
          address: 'test address',
          city: 'test city',
          postCode: '0000',
          dob: '00/00/0000',
          mobileNumber: '0000000',
          email: 'test@test.com',
          occupation: 'test occupation'
        };
      });

      $scope.create = function () {
        $clients.newClient($scope.data, function (err) {
          if (err) {
            $ionicPopup.alert({
              title: "Error!",
              template: err.message
            }).then(function () {

            });

            return;
          }

          $scope.data = {};

          $ionicPopup.alert({
            title: "Client saved",
            template: 'New client has been saved!'
          }).then(function () {
            $state.go('app.blocks.client');
          });
        });
      };

      $scope.choose = function () {
        var buttons = [
          {text: 'Friend'},
          {text: 'Family'},
          {text: 'Work Colleague'},
          {text: 'Walk-in'},
          {text: 'Internet'},
          {text: 'Social media'}
        ];

        $ionicActionSheet.show({
          buttons: buttons,
          titleText: 'How did you hear about us?',
          cancelText: 'Cancel',
          cancel: function () {
          },
          buttonClicked: function (index) {
            $scope.data.howHear = buttons[index].text;
            return true;
          }
        });
      };
    }
  ]);

  app.controller('AnalysisCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        // logical
        $scope.vars = $scope.vars || {};
        $scope.vars.blockIndex = -1;
        $scope.vars.analysisData = {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
        $scope.vars.analysisData = {};

        $('.analysis-block').removeClass('selected');
      });

      $scope.blockClick = function(index) {
        markAnalysisBlock(index);
      };

      $scope.analysisChosen = function(name) {
        if ($scope.vars.blockIndex < 0) return;

        $scope.vars.analysisData[name] = !$scope.vars.analysisData[name];
      };

      function markAnalysisBlock(index) {
        // prepare
        $scope.vars.data.analysis = $scope.vars.data.analysis || [];

        // logical
        $scope.vars.blockIndex = index;
        $scope.vars.analysisData = $scope.vars.data.analysis[index] || {};
        $scope.vars.data.analysis[index] = $scope.vars.analysisData;

        // visual
        $('.analysis-block').removeClass('selected');
        $('.analysis-block-' + (index + 1)).addClass('selected');
      }
    }
  ]);

  app.controller('PrescriptionCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
        switchTab('morning');
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });

      $scope.morning = function () {
        switchTab('morning');
      };

      $scope.night = function () {
        switchTab('night');
      };

      function switchTab(name) {
        // prepare
        $scope.vars.data.prescription = $scope.vars.data.prescription || {};

        // logical
        $scope.tab = name;

        $scope.vars.activeData = $scope.vars.data.prescription[name] || {};
        $scope.vars.data.prescription[name] = $scope.vars.activeData;
      }
    }
  ]);

  app.controller('ProgrammeCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

  app.controller('PhotosCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

  app.controller('HistoryCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
        $scope.vars.data.history = $scope.vars.data.history || [];
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });

      $scope.new = function() {

      };
    }
  ]);

  app.controller('NotesCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

  app.controller('InstructCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

  app.controller('AlertsCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

  app.controller('SkinfpCtrl', ['$scope', '$state', '$clients',
    function ($scope, $state, $clients) {
      $scope.$on('$ionicView.enter', function () {
        if (!$clients.getCurrent()) {
          $state.go('app');
          return;
        }

        $scope.vars = $scope.vars || {};
        $scope.vars.data = $clients.getCurrent();
      });

      $scope.$on('$ionicView.beforeLeave', function () {
        $clients.saveClients();
      });
    }
  ]);

})();
