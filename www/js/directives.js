(function() {
  "use strict";

  var app = angular.module('consultBeauty');

  app.directive("cbFullHeight", [function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.css('height', window.innerHeight + 'px');
      }
    };
  }]);

  app.directive("cbContentHeight", [function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.css('height', (window.innerHeight - 64) + 'px');
      }
    };
  }]);

  app.directive("cbBack", ['$ionicHistory',
    function($ionicHistory) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            $ionicHistory.goBack();
          });
        }
      };
    }
  ]);

  app.directive("cbHome", ['$state',
    function($state) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            $state.go('app');
          });
        }
      };
    }
  ]);

  app.directive("cbUser", ['$state',
    function($state) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            $state.go('app.account');
          });
        }
      };
    }
  ]);

  app.directive("cbClients", ['$user', '$clients', '$state',
    function($user, $clients, $state) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var dataUpdated = function() {
            var clientCount = $clients.count();
            var clientLimit = $user.getClientLimit();

            element.text(' ' + clientCount + '/' + clientLimit);
          };

          $user.registerObserverCallback(dataUpdated);
          $clients.registerObserverCallback(dataUpdated);

          element.on('click', function() {
            $state.go('app.subscriptions');
          });

          scope.$on('$destroy', function() {
            $user.deregisterObserverCallback(dataUpdated);
            $clients.deregisterObserverCallback(dataUpdated);
          });

          dataUpdated();
        }
      };
    }
  ]);

  app.directive("cbSignout", ['$user', '$state', '$ionicPopup',
    function($user, $state, $ionicPopup) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            var confirmPopup = $ionicPopup.confirm({
              title: 'Sure ?',
              template: 'Are you sure you want to sign out ?'
            });

            confirmPopup.then(function(res) {
              if (res) {
                $user.signOut();
                $state.go('login');
              }
            });
          });
        }
      };
    }
  ]);

  app.directive("cbTopButtons", ['$rootScope', function($rootScope) {
    return {
      restrict: 'E',
      templateUrl: 'partials/top_buttons.html',
      link: function(scope, element, attrs) {
        scope.disabled = !$rootScope.currentClient;

        $rootScope.$watch('currentClient', function() {
          scope.disabled = !$rootScope.currentClient;
        });
      }
    };
  }]);

  app.directive("cbBottomButtons", ['$rootScope', function($rootScope) {
    return {
      restrict: 'E',
      templateUrl: 'partials/bottom_buttons.html',
      link: function(scope, element, attrs) {
        scope.disabled = !$rootScope.currentClient;

        $rootScope.$watch('currentClient', function() {
          scope.disabled = !$rootScope.currentClient;
        });
      }
    };
  }]);

  app.directive("cbPanelButton", ['$state',
    function($state) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            var action = attrs.cbAction;

            switch (action) {
              case 'client':
                $state.go('app.blocks.client');
                break;
              case 'analysis':
                $state.go('app.blocks.analysis');
                break;
              case 'prescription':
                $state.go('app.blocks.prescription');
                break;
              case 'programme':
                $state.go('app.blocks.programme');
                break;
              case 'new':
                $state.go('app.blocks.new');
                break;
              case 'print':

                break;
              case 'photos':
                $state.go('app.blocks.photos');
                break;
              case 'history':
                $state.go('app.blocks.history');
                break;
              case 'notes':
                $state.go('app.blocks.notes');
                break;
              case 'instruct':
                $state.go('app.blocks.instruct');
                break;
              case 'alerts':
                $state.go('app.blocks.alerts');
                break;
              case 'skinfp':
                $state.go('app.blocks.skinfp');
                break;
            }
          });
        }
      };
    }
  ]);

  app.directive("compareTo", function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {
        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
  });

  app.directive('cbAutoresize', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        jQuery(element).each(function() {
          var offset = this.offsetHeight - this.clientHeight;

          var resizeTextarea = function(el) {
            jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
          };

          jQuery(this).on('keyup input', function() {
            resizeTextarea(this);
          });
        });
      }
    };
  });

})();
